import 'dart:convert';
import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart' as http;

class DadataApi {
  static const endPointUrl = 'https://suggestions.dadata.ru/suggestions/api';
  static const apiKey = 'aebbb3a2edc89c9e021ea238a8c6def114c39ef6';
  late http.Client httpClient;

  DadataApi() {
    httpClient = http.Client();
  }

  Future<String> getCityName({required Position position}) async {
    double lat = position.latitude;
    double lon = position.longitude;
    final requestUrl =
        '$endPointUrl/4_1/rs/geolocate/address?lat=$lat&lon=$lon';
    final response = await httpClient.get(Uri.parse(requestUrl), headers: {
      "Accept": 'application/json',
      "Authorization": 'Token $apiKey'
    });

    if (response.statusCode != 200) {
      throw Exception(
          'ошибка получения информации для локации $lat/$lon: ${response.statusCode}');
    }

    return jsonDecode(response.body)['suggestions'][0]['data']['city'];
  }
}
