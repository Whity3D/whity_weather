import 'dart:convert';

import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart' as http;
import 'package:whity_weather/models/forecast.dart';
import 'package:whity_weather/models/location.dart';
import 'package:whity_weather/services/weather_api.dart';

class OpenWeatherApi extends WeatherApi {
  static const endPointUrl = 'https://api.openweathermap.org/data/2.5';
  static const apiKey = "07e734556cb78597051a85d9b6f35ee5";
  late http.Client httpClient;

  OpenWeatherApi() {
    httpClient = http.Client();
  }

  @override
  Future<Location> getLocation(String cityName) async {
    final requestUrl = '$endPointUrl/weather?q=$cityName&APPID=$apiKey';
    final response = await httpClient.get(Uri.parse(requestUrl));

    if (response.statusCode != 200) {
      throw Exception(
          'ошибка получения информации для города $cityName: ${response.statusCode}');
    }

    return Location.fromJson(jsonDecode(response.body));
  }

  @override
  Future<Forecast> getWeather(Location location, String cityName, Position? position) async {
    final requestUrl =
        '$endPointUrl/onecall?lat=${location.latitude}&lon=${location.longitude}&units=metric&lang=ru&exclude=hourly,minutely&APPID=$apiKey';
    final response = await httpClient.get(Uri.parse(requestUrl));

    if (response.statusCode != 200) {
      throw Exception('ошибка получения погоды: ${response.statusCode}');
    }

    return Forecast.fromJson(jsonDecode(response.body), cityName, position);
  }
}
