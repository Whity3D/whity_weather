import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:whity_weather/models/weather.dart';
import 'package:whity_weather/viewmodels/city_entry_view_model.dart';
import 'package:whity_weather/viewmodels/forecast_view_model.dart';
import 'package:whity_weather/widgets/city_entry_widget.dart';
import 'package:whity_weather/widgets/daily_summary_widget.dart';
import 'package:whity_weather/widgets/gradient_container_widget.dart';
import 'package:whity_weather/widgets/last_updated_widget.dart';
import 'package:whity_weather/widgets/location_widget.dart';
import 'package:whity_weather/widgets/weather_description_widget.dart';
import 'package:whity_weather/widgets/weather_summary_widget.dart';

class HomeScreenWidget extends StatelessWidget {
  const HomeScreenWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<ForecastViewModel>(
        builder: (context, model, child) => Scaffold(
              body: _buildGradientContainer(
                  model.condition, model.isDayTime, buildHomeView(context)),
            ));
  }

  Widget buildHomeView(BuildContext context) {
    return Consumer<ForecastViewModel>(
        builder: (context, weatherViewModel, child) => SizedBox(
              height: MediaQuery.of(context).size.height,
              child: RefreshIndicator(
                color: Colors.transparent,
                backgroundColor: Colors.transparent,
                onRefresh: () => refreshWeather(weatherViewModel, context),
                child: ListView(
                  children: [
                    const CityEntryWidget(),
                    weatherViewModel.isRequestPending
                        ? buildBusyIndicator()
                        : weatherViewModel.isRequestError
                            ? const Center(
                                child: Text(
                                  'Что-то пошло не так',
                                  style: TextStyle(
                                      fontSize: 21, color: Colors.white),
                                ),
                              )
                            : Column(
                                children: [
                                  LocationWidget(
                                    longitude: weatherViewModel.longitude,
                                    latitude: weatherViewModel.latitude,
                                    cityName: weatherViewModel.cityName,
                                  ),
                                  const SizedBox(
                                    height: 50,
                                  ),
                                  WeatherSummaryWidget(
                                      condition: weatherViewModel.condition,
                                      temp: weatherViewModel.temp,
                                      feelsLike: weatherViewModel.feelsLike,
                                      isDayTime: weatherViewModel.isDayTime),
                                  const SizedBox(
                                    height: 20,
                                  ),
                                  WeatherDescriptionWidget(
                                      weatherDescription:
                                          weatherViewModel.description),
                                  const SizedBox(
                                    height: 140,
                                  ),
                                  buildDailySummary(weatherViewModel.daily),
                                  LastUpdatedWidget(
                                      lastUpdatedOn:
                                          weatherViewModel.lastUpdated)
                                ],
                              )
                  ],
                ),
              ),
            ));
  }

  Widget buildBusyIndicator() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: const [
        CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
        ),
        SizedBox(
          height: 20,
        ),
        Text('Подождите...',
            style: TextStyle(
                fontSize: 18, fontWeight: FontWeight.w300, color: Colors.white))
      ],
    );
  }

  Widget buildDailySummary(List<Weather> dailyForecast) {
    return SizedBox(
      height: 100,
      child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: dailyForecast.length,
          itemBuilder: (BuildContext context, int index) {
            return DailySummaryWidget(
              weather: dailyForecast[index],
            );
          }),
    );
  }

  Future<void> refreshWeather(
      ForecastViewModel weatherVM, BuildContext context) {
    String city =
        Provider.of<CityEntryViewModel>(context, listen: false).cityName;
    return weatherVM.getLatestWeather(city);
  }

  GradientContainerWidget _buildGradientContainer(
      WeatherCondition condition, bool isDayTime, Widget child) {
    GradientContainerWidget container;

    if (isDayTime != null && !isDayTime) {
      container = GradientContainerWidget(color: Colors.blueGrey, child: child);
    } else {
      switch (condition) {
        case WeatherCondition.clear:
        case WeatherCondition.lightCloud:
          container =
              GradientContainerWidget(color: Colors.yellow, child: child);
          break;
        case WeatherCondition.fog:
        case WeatherCondition.atmosphere:
        case WeatherCondition.rain:
        case WeatherCondition.drizzle:
        case WeatherCondition.mist:
        case WeatherCondition.heavyCloud:
          container =
              GradientContainerWidget(color: Colors.indigo, child: child);
          break;
        case WeatherCondition.snow:
          container =
              GradientContainerWidget(color: Colors.lightBlue, child: child);
          break;
        case WeatherCondition.thunderstorm:
          container =
              GradientContainerWidget(color: Colors.deepPurple, child: child);
          break;
        case WeatherCondition.unknown:
          container =
              GradientContainerWidget(color: Colors.lightBlue, child: child);
      }
    }

    return container;
  }
}
