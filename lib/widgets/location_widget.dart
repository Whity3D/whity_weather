import 'package:flutter/material.dart';

class LocationWidget extends StatelessWidget {
  final double longitude;
  final double latitude;
  final String cityName;

  const LocationWidget(
      {Key? key,
      required this.longitude,
      required this.latitude,
      required this.cityName})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: [
          Text(
            cityName.toUpperCase(),
            style: const TextStyle(
              fontSize: 40,
              fontWeight: FontWeight.w600,
              color: Colors.white,
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Icon(
                Icons.location_on,
                color: Colors.white,
                size: 15,
              ),
              const SizedBox(
                width: 10,
              ),
              Text(
                longitude.toString(),
                style: const TextStyle(fontSize: 16, color: Colors.white),
              ),
              const Text(
                ' , ',
                style: TextStyle(fontSize: 16, color: Colors.white),
              ),
              Text(
                latitude.toString(),
                style: const TextStyle(fontSize: 16, color: Colors.white),
              )
            ],
          )
        ],
      ),
    );
  }
}
