import 'package:flutter/material.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:whity_weather/models/weather.dart';

class DailySummaryWidget extends StatelessWidget {
  final Weather weather;

  const DailySummaryWidget({Key? key, required this.weather}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const langCode = "ru";
    initializeDateFormatting(langCode);
    final dayOfWeek =
        toBeginningOfSentenceCase(DateFormat('EEE', langCode).format(weather.date));

    return Padding(
      padding: const EdgeInsets.all(15),
      child: Row(
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Text(
                dayOfWeek ?? '',
                textAlign: TextAlign.center,
                style: const TextStyle(
                  fontSize: 18,
                  color: Colors.white,
                  fontWeight: FontWeight.w300,
                ),
              ),
              Text(
                weather.temp.round().toString(),
                textAlign: TextAlign.center,
                style: const TextStyle(
                    fontSize: 20,
                    color: Colors.white,
                    fontWeight: FontWeight.w500),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(left: 5),
            child: _mapWeatherConditionToImage(weather.condition),
          )
        ],
      ),
    );
  }

  Widget _mapWeatherConditionToImage(WeatherCondition condition) {
    Image image;
    switch (condition) {
      case WeatherCondition.clear:
        image = Image.asset('assets/images/clear_small.png');
        break;
      case WeatherCondition.rain:
        image = Image.asset('assets/images/rain_small.png');
        break;
      case WeatherCondition.snow:
        image = Image.asset('assets/images/snow_small.png');
        break;
      case WeatherCondition.atmosphere:
        image = Image.asset('assets/images/atmosphere_small.png');
        break;
      case WeatherCondition.drizzle:
        image = Image.asset('assets/images/drizzle_small.png');
        break;
      case WeatherCondition.mist:
        image = Image.asset('assets/images/mist_small.png');
        break;
      case WeatherCondition.fog:
        image = Image.asset('assets/images/fog_small.png');
        break;
      case WeatherCondition.thunderstorm:
        image = Image.asset('assets/images/thunderstorm_small.png');
        break;
      case WeatherCondition.lightCloud:
        image = Image.asset('assets/images/light_cloud_small.png');
        break;
      case WeatherCondition.heavyCloud:
        image = Image.asset('assets/images/cloudy_small.png');
        break;
      case WeatherCondition.unknown:
        image = Image.asset('assets/images/atmosphere_small.png');
        break;
      default:
        image = Image.asset('assets/images/atmosphere_small.png');
    }

    return Padding(
      padding: const EdgeInsets.only(top: 5),
      child: image,
    );
  }
}
