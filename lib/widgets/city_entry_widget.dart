import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:whity_weather/navigation/main_navigation.dart';
import 'package:whity_weather/viewmodels/city_entry_view_model.dart';

class CityEntryWidget extends StatefulWidget {
  const CityEntryWidget({Key? key}) : super(key: key);

  @override
  _CityEntryState createState() => _CityEntryState();
}

class _CityEntryState extends State<CityEntryWidget> {
  TextEditingController cityNameController = TextEditingController();

  @override
  void initState() {
    super.initState();

    cityNameController.addListener(() {
      Provider.of<CityEntryViewModel>(context, listen: false)
          .updateCity(cityNameController.text);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<CityEntryViewModel>(
        builder: (context, model, child) => Container(
              margin: const EdgeInsets.only(
                  left: 20, top: 20, right: 20, bottom: 50),
              padding:
                  const EdgeInsets.only(left: 5, top: 5, right: 20, bottom: 00),
              height: 50,
              width: 200,
              decoration: BoxDecoration(
                  color: Theme.of(context).colorScheme.background,
                  borderRadius: const BorderRadius.all(Radius.circular(3)),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black.withOpacity(0.3),
                      spreadRadius: 3,
                      blurRadius: 5,
                      offset: const Offset(0, 3),
                    )
                  ]),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  IconButton(
                      onPressed: () {
                        model.updateCity(cityNameController.text);
                        model.refreshWeather(cityNameController.text, context);
                      },
                      icon: const Icon(Icons.search)),
                  const SizedBox(
                    width: 10,
                  ),
                  Expanded(
                      child: TextField(
                    controller: cityNameController,
                    decoration: InputDecoration.collapsed(
                      filled: true,
                      fillColor: Theme.of(context).colorScheme.background,
                      hintText: 'Введите город',
                    ),
                    onSubmitted: (String cityName) =>
                        {model.refreshWeather(cityName, context)},
                  )),
                  const SizedBox(
                    width: 5,
                  ),
                  IconButton(
                      onPressed: () {
                        Navigator.of(context)
                            .pushNamed(MainNavigationRoutesNames.settings);
                      },
                      icon: const Icon(Icons.menu)),
                ],
              ),
            ));
  }
}
