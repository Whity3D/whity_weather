import 'package:flutter/material.dart';
import 'package:whity_weather/models/weather.dart';

class WeatherSummaryWidget extends StatelessWidget {
  final WeatherCondition condition;
  final double temp;
  final double feelsLike;
  final bool isDayTime;

  const WeatherSummaryWidget(
      {Key? key,
      required this.condition,
      required this.temp,
      required this.feelsLike,
      required this.isDayTime})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        mainAxisSize: MainAxisSize.max,
        children: [
          Column(
            children: [
              Text(
                '${_formatTemperature(temp)} \u2103',
                style: const TextStyle(
                  fontSize: 50,
                  color: Colors.white,
                  fontWeight: FontWeight.w300,
                ),
              ),
              Text(
                'Ощущается ${_formatTemperature(feelsLike)} \u2103',
                style: const TextStyle(
                  fontSize: 18,
                  color: Colors.white,
                  fontWeight: FontWeight.w300,
                ),
              )
            ],
          ),
          _mapWeatherConditionToImage(condition, isDayTime),
        ],
      ),
    );
  }

  String _formatTemperature(double temp) {
    var temperature = (temp == null ? '' : temp.round().toString());
    return temperature;
  }

  Widget _mapWeatherConditionToImage(
      WeatherCondition condition, bool isDayTime) {
    Image image;
    switch (condition) {
      case WeatherCondition.clear:
        image = Image.asset('assets/images/clear.png');
        break;
      case WeatherCondition.rain:
        image = Image.asset('assets/images/rain.png');
        break;
      case WeatherCondition.snow:
        image = Image.asset('assets/images/snow.png');
        break;
      case WeatherCondition.atmosphere:
        image = Image.asset('assets/images/atmosphere.png');
        break;
      case WeatherCondition.drizzle:
        image = Image.asset('assets/images/drizzle.png');
        break;
      case WeatherCondition.mist:
        image = Image.asset('assets/images/mist.png');
        break;
      case WeatherCondition.fog:
        image = Image.asset('assets/images/fog.png');
        break;
      case WeatherCondition.thunderstorm:
        image = Image.asset('assets/images/thunderstorm.png');
        break;
      case WeatherCondition.lightCloud:
        image = Image.asset('assets/images/light_cloud.png');
        break;
      case WeatherCondition.heavyCloud:
        image = Image.asset('assets/images/cloudy.png');
        break;
      case WeatherCondition.unknown:
        image = Image.asset('assets/images/atmosphere.png');
        break;
      default:
        image = Image.asset('assets/images/atmosphere.png');
    }

    return Padding(padding: const EdgeInsets.only(top: 5), child: image);
  }
}
