import 'package:geolocator/geolocator.dart';
import 'package:whity_weather/models/weather.dart';

class Forecast {
  final DateTime lastUpdated;
  final double longitude;
  final double latitude;
  final List<Weather> daily;
  final Weather current;
  final bool isDayTime;
  String? cityName;

  Forecast(
      {required this.lastUpdated,
      required this.longitude,
      required this.latitude,
      required this.daily,
      required this.current,
      required this.isDayTime,
      this.cityName});

  static Forecast fromJson(dynamic json, String cityName, Position? position) {
    var weather = json['current']['weather'][0];
    var date = DateTime.fromMicrosecondsSinceEpoch(json['current']['dt'] * 1000,
        isUtc: true);
    var sunrise = DateTime.fromMicrosecondsSinceEpoch(
        json['current']['sunrise'] * 1000,
        isUtc: true);
    var sunset = DateTime.fromMillisecondsSinceEpoch(
        json['current']['sunset'] * 1000,
        isUtc: true);

    bool isDay = date.isAfter(sunrise) && date.isBefore(sunset);

    bool hasDaily = json['daily'] != null;
    List<Weather> tempDaily = [];
    if (hasDaily) {
      List items = json['daily'];
      tempDaily = items
          .map((item) => Weather.fromDailyJson(item))
          .toList()
          .skip(1)
          .toList();
    }

    var currentForecast = Weather(
        condition: Weather.mapStringToCondition(
            weather['main'], int.parse(json['current']['clouds'].toString())),
        description: weather['description'],
        temp: json['current']['temp'].toDouble(),
        feelLikeTemp: json['current']['feels_like'],
        cloudiness: int.parse(json['current']['clouds'].toString()),
        date: date);

    return Forecast(
        lastUpdated: DateTime.now(),
        longitude: position?.longitude ?? json['lon'].toDouble(),
        latitude: position?.latitude ?? json['lat'].toDouble(),
        daily: tempDaily,
        current: currentForecast,
        isDayTime: isDay,
        cityName: cityName);
  }
}
