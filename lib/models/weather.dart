import 'package:whity_weather/utils/strings.dart';

enum WeatherCondition {
  clear,
  rain,
  snow,
  atmosphere,
  drizzle,
  mist,
  fog,
  thunderstorm,
  lightCloud,
  heavyCloud,
  unknown
}

class Weather {
  final WeatherCondition condition;
  final String description;
  final double temp;
  final double feelLikeTemp;
  final int cloudiness;
  final DateTime date;

  Weather(
      {required this.condition,
      required this.description,
      required this.temp,
      required this.feelLikeTemp,
      required this.cloudiness,
      required this.date});

  static Weather fromDailyJson(dynamic daily) {
    var cloudiness = daily['clouds'];
    var weather = daily['weather'][0];

    return Weather(
        condition: mapStringToCondition(weather['main'], cloudiness),
        description: (weather['description'].toString().toTitleCase()),
        cloudiness: cloudiness,
        temp: daily['temp']['day'].toDouble(),
        date:
            DateTime.fromMillisecondsSinceEpoch(daily['dt'] * 1000, isUtc: true),
        feelLikeTemp: daily['feels_like']['day'].toDouble());
  }

  static WeatherCondition mapStringToCondition(String input, int cloudiness) {
    WeatherCondition condition;
    switch (input) {
      case 'Clear':
        condition = WeatherCondition.clear;
        break;
      case 'Rain':
        condition = WeatherCondition.rain;
        break;
      case 'Snow':
        condition = WeatherCondition.snow;
        break;
      case 'Smoke':
      case 'Haze':
      case 'Dust':
      case 'Sand':
      case 'Ash':
      case 'Squall':
      case 'Tornado':
        condition = WeatherCondition.atmosphere;
        break;
      case 'Drizzle':
        condition = WeatherCondition.drizzle;
        break;
      case 'Mist':
        condition = WeatherCondition.mist;
        break;
      case 'fog':
        condition = WeatherCondition.fog;
        break;
      case 'Thunderstorm':
        condition = WeatherCondition.thunderstorm;
        break;
      case 'Clouds':
        condition = (cloudiness >= 85)
            ? WeatherCondition.heavyCloud
            : WeatherCondition.lightCloud;
        break;
      default:
        condition = WeatherCondition.unknown;
    }

    return condition;
  }
}
