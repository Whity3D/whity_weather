import 'package:geolocator/geolocator.dart';
import 'package:whity_weather/api/dadata_api.dart';

class DadataService {
  final DadataApi dadataApi = DadataApi();

  DadataService();

  Future<String> getCityName(Position position) {
    return dadataApi.getCityName(position: position);
  }
}
