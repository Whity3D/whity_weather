import 'package:geolocator/geolocator.dart';
import 'package:whity_weather/models/forecast.dart';
import 'package:whity_weather/models/location.dart';
import 'package:whity_weather/services/weather_api.dart';

class ForecastService {
  final WeatherApi weatherApi;

  ForecastService(this.weatherApi);

  Future<Forecast> getWeather(String cityName, Position? position) async {
    final location = position != null
        ? Location(
            longitude: position.longitude,
            latitude: position.latitude)
        : await weatherApi.getLocation(cityName);
    return await weatherApi.getWeather(location, cityName, position);
  }
}
