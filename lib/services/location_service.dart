import 'package:geolocator/geolocator.dart';

class LocationService {
  Future<Position> getPosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();

    if (serviceEnabled) {
      permission = await Geolocator.checkPermission();
      if (permission == LocationPermission.denied) {
        permission = await Geolocator.requestPermission();
        if (permission == LocationPermission.denied) {
          return Future.error('Доступ к геолокации запрещен');
        }
      }
      if (permission == LocationPermission.deniedForever) {
        return Future.error(
            'Доступ к геолокации запрещён навсегда, мы не можем запросить разрешение');
      }
      print(permission);

      return Geolocator.getCurrentPosition();
    }

    return Future.error('Геолокация отключена');
  }
}
