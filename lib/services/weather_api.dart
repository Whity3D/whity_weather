import 'package:geolocator/geolocator.dart';
import 'package:whity_weather/models/forecast.dart';
import 'package:whity_weather/models/location.dart';

abstract class WeatherApi {
  Future<Forecast> getWeather(Location location, String cityName, Position? position);
  Future<Location> getLocation(String cityName);
}