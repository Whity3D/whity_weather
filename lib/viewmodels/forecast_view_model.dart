import 'package:flutter/cupertino.dart';
import 'package:geolocator/geolocator.dart';
import 'package:whity_weather/api/openweather_api.dart';
import 'package:whity_weather/models/forecast.dart';
import 'package:whity_weather/models/weather.dart';
import 'package:whity_weather/services/dadata_service.dart';
import 'package:whity_weather/services/forecast_service.dart';
import 'package:whity_weather/services/location_service.dart';
import 'package:whity_weather/utils/strings.dart';

class ForecastViewModel with ChangeNotifier {
  bool isRequestPending = true;
  bool isWeatherLoaded = false;
  bool isRequestError = false;

  WeatherCondition _condition = WeatherCondition.unknown;
  late String _description;
  late double _minTemp;
  late double _maxTemp;
  late double _temp;
  late double _feelsLike;
  late int _locationId;
  late DateTime _lastUpdated;
  late String _cityName;
  late double _latitude;
  late double _longitude;
  late List<Weather> _daily;
  bool _isDayTime = false;

  WeatherCondition get condition => _condition;

  String get description => _description;

  double get minTemp => _minTemp;

  double get maxTemp => _maxTemp;

  double get temp => _temp;

  double get feelsLike => _feelsLike;

  int get locationId => _locationId;

  DateTime get lastUpdated => _lastUpdated;

  String get cityName => _cityName;

  double get latitude => _latitude;

  double get longitude => _longitude;

  bool get isDayTime => _isDayTime;

  List<Weather> get daily => _daily;

  late ForecastService forecastService;

  ForecastViewModel() {
    forecastService = ForecastService(OpenWeatherApi());
  }

  Future<Forecast> getLatestWeather(String cityName) async {
    LocationService locationService = LocationService();
    DadataService dadataService = DadataService();
    setRequestPendingState(true);
    isRequestError = false;

    Position position = await locationService.getPosition();

    if (position != null) {
      String cName = await dadataService.getCityName(position);

      print(cName);
      cityName = cName;
    }

    print(position);


    Forecast? latest;
    try {
      await Future.delayed(const Duration(seconds: 1), () => {});
      latest = await forecastService
          .getWeather( cityName, position)
          .catchError((onError) => isRequestError = true);
    } catch (error) {
      isRequestError = true;
    }
    if (latest == null) {
      throw Exception('');
    }

    isWeatherLoaded = true;
    updateModel(latest, cityName);
    setRequestPendingState(false);
    notifyListeners();
    return latest;
  }

  void setRequestPendingState(bool isPending) {
    isRequestPending = isPending;
    notifyListeners();
  }

  void updateModel(Forecast forecast, String cityName) {
    if (isRequestError) return;

    _condition = forecast.current.condition;
    _cityName = (forecast.cityName ?? '').toTitleCase();
    _description = forecast.current.description.toTitleCase();
    _lastUpdated = forecast.lastUpdated;
    _temp = forecast.current.temp;
    _feelsLike = forecast.current.feelLikeTemp;
    _longitude = forecast.longitude;
    _latitude = forecast.latitude;
    _daily = forecast.daily;
    _isDayTime = forecast.isDayTime;
  }
}
