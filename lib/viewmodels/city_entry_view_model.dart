import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:whity_weather/viewmodels/forecast_view_model.dart';

class CityEntryViewModel with ChangeNotifier {
  late String _cityName;

  CityEntryViewModel();

  String get cityName => _cityName;

  void refreshWeather(String newCityName, BuildContext context) {
    Provider.of<ForecastViewModel>(context, listen: false)
        .getLatestWeather(_cityName);

    notifyListeners();
  }

  void updateCity(String newCityName) {
    _cityName = newCityName;
  }
}
