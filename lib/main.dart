import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:whity_weather/viewmodels/city_entry_view_model.dart';
import 'package:whity_weather/viewmodels/forecast_view_model.dart';

import 'widgets/app.dart';

void main() {
  runApp(MultiProvider(
    providers: [
      ChangeNotifierProvider<CityEntryViewModel>(
          create: (_) => CityEntryViewModel()),
      ChangeNotifierProvider<ForecastViewModel>(
          create: (_) => ForecastViewModel()),
    ],
    child: const MyApp(),
  ));
}


