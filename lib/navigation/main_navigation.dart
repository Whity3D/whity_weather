import 'package:flutter/material.dart';
import 'package:whity_weather/screens/home_screen.dart';
import 'package:whity_weather/screens/setting_screen.dart';

abstract class MainNavigationRoutesNames {
  static const mainScreen = '/main_screen';
  static const settings = '/settings';
}

class MainNavigation {
  final routes = <String, Widget Function(BuildContext)>{
    MainNavigationRoutesNames.mainScreen: (_) => const HomeScreenWidget(),
    MainNavigationRoutesNames.settings: (_) => const SettingsScreenWidget(),
  };

  final initialRoute = MainNavigationRoutesNames.mainScreen;
}
